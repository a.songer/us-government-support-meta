Title: Coverage for <day>

/label ~"Coverage"
/label ~"Awaiting feedback" 

| Name | Month/Day | Covered? | New Assignee    | 
|------|-------|-------|----------|
| NAME  |   DATE  [AM\|PM]  |   [ ]    |   |


/label ~"FY25 OKR::Not Applicable" 
/label ~"team::US-Gov-Support" 
